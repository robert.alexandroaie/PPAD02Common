package ac.dc.ppad.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface InterfaceInfo extends Remote{

	String getSomething(String source) throws RemoteException;
}
